# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Meta package for coreapps"
HOMEPAGE="https://gitlab.com/cubocore/coreapps"
SRC_URI=""

LICENSE="metapackage"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="
	~gui-apps/corefm-${PV}
	~gui-apps/coreimage-${PV}
	~gui-apps/corekeyboard-${PV}
	~gui-apps/corestuff-${PV}
	~gui-apps/coreterminal-${PV}
"
