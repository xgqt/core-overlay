# Core Overlay


## About

Core Overlay is a Gentoo overlay for programs by [CuboCore](https://gitlab.com/cubocore)


## Showcase

[CoreStats](https://gitlab.com/cubocore/coreapps/corestats/), [CoreFM](https://gitlab.com/cubocore/coreapps/corefm/) and [CoreAction](https://gitlab.com/cubocore/coreapps/coreaction/) running alongside [XFCE4](https://xfce.org/)

<p align="center">
    <img src="./showcase.jpg">
</p>


## Adding the overlay

#### Manual

##### Layman

If you are using [Layman](https://wiki.gentoo.org/wiki/Layman), execute:

``` sh
layman -o 'https://gitlab.com/src_prepare/src_prepare-overlay/raw/master/repositories.xml' -f -a src_prepare-overlay
```


## How do I sync this?

Execute:

``` sh
emaint sync -r core-overlay
```
